<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Translatable;


class News extends Model
{
  protected $translatable = ['text', 'name'];
}
